require 'rgl/dot'
require 'rgl/rdot'
require './color_vertex'

module COLORING

  @max_color
  attr_accessor  :max_color
  class UnDirectedEdgeColor < RGL::Edge::UnDirectedEdge
      attr_accessor :color_id

  end

  def edge_class
    return UnDirectedEdgeColor
  end

  @vertex_counter

  def say_hello
    puts 'hello'
  end

  def my_size
      puts self.size
  end

  def vertex_degree

    each_vertex {|v|

      puts v.to_s + ' ' + out_degree(v).to_s
    }
  end

  def sort_vertices_down
    verteice = @vertice_dict.keys
    verteice.sort_by! {|v| out_degree(v)}.reverse!
  end

  def brutal_force_painter(v)
     color = 1
     adjacent_colors = self.adjacent_vertices(v).map{|v| v.color_id}.compact || []
     while adjacent_colors.include? color
          color += 1
     end

    v.color_id = color
    @max_color = color if @max_color.nil? || color > @max_color
  end


  # Algorytm LF (largest first)[edytuj]
  # Kolorowanie grafu za pomocą algorytmu LF można opisać następująco:
  # Uporządkuj wierzchołki grafu malejąco według ich stopni (liczby krawędzi z nich wychodzących).
  # Koloruj wierzchołki zachłannie, zgodnie z ustaloną wcześniej kolejnością (zaczynając od wierzchołka o największym stopniu).
  def lf_coloring
    sort_vertices = sort_vertices_down
    sort_vertices.each {|v|
      self.brutal_force_painter v
    }
  end


  # Return a RGL::DOT::Digraph for directed graphs or a DOT::Subgraph for an
  # undirected Graph.  _params_ can contain any graph property specified in
  # rdot.rb.

  def to_dot_graph (params = {})
    params['name'] ||= self.class.name.gsub(/:/,'_')
    fontsize   = params['fontsize'] ? params['fontsize'] : '8'
    graph      = RGL::DOT::Graph.new(params)
    edge_class = RGL::DOT::Edge

    each_edge do |u,v|
      unless u.type == 'edge' || v.type == 'edge'
        color_for_edge(u,v)
        graph << edge_class.new('from'     => u.to_s,
                                'to'       => v.to_s,
                                'label' => color_for_edge(u,v),
                                'color' => color_for_edge(u,v)
          )
      end
    end

    each_vertex do |v|
      unless v.type == 'edge'
        asterix = ''
        asterix = '*' if v.type == 'edge'
        name = v.to_s
        color = v.color_id.to_s
        graph << RGL::DOT::Node.new('name'     => name,
                                    'label'    => name.to_s + asterix+ '(' + color.to_s + ')',
                                    'color'    =>  color.to_s
        )
      end
    end

    graph
  end


  # dodaje wierzcholki zgodnie z algorytmem zamieniania problemu kolorowania totalnego na kolorawanie wierzcholkow
  def transform_graph

   idx = 100
   edges = []
   self.each_edge {|v,u|
      w = ColorVertex.new(idx)
      w.type = 'edge'
      idx += 1
      edges << [v,w]
      edges << [w,u]

   }

     edges.each {|e|
       self.add_edges(e)
     }


    edges2 = []
    each_vertex {|v|
      edgeVertices = getEdgeVerticesFrom(v)
      if edgeVertices.length > 1
        edgeVertices.combination(2) {|c|
          edges2 << [c[0], c[1]]
        }
      end

      edges2.each {|e|
        self.add_edges(e)
      }

    }

  end


  def color_for_edge(u,v)
    vertices = getEdgeVertices
    color = vertices.select { |w|
      adj = []
      each_adjacent(w) {|ad|
         adj << ad if ad.type != 'edge'
      }
      !adj.index(u).nil? && !adj.index(v).nil?
    }


     return color.first.color_id unless color.first.nil?
     ''

  end

  def getEdgeVertices
    vertices = []
    each_vertex {|v|
      vertices << v if v.type == 'edge'
    }
    vertices
  end

  def getEdgeVerticesFrom(vertex)
    vertices = []
    each_adjacent(vertex) {|v|
      vertices << v if v.type == 'edge'
    }
    vertices

  end




  def load(file)
    options = ''
    vertices = []
    edges = []

    File.open(file, 'r') do |f|
      while (line = f.gets)
        edge = line[/(\d) -- (\d)/]
        vertex = line[/(\d) [^-]+/]

        unless edge.nil?
          v1 = edge[0].to_i
          v2 = edge[-1].to_i
          vertices[v1] ||= ColorVertex.new(v1) unless v1.nil?
          vertices[v2] ||= ColorVertex.new(v2) unless v2.nil?
          edges << [vertices[v1],vertices[v2]]


        end

      end
      vertices.compact!
      edges.compact!
      edges.each {|e|
        self.add_edges(e)
      }


    end

  end


  def write_to_graphic_file (fmt='png', dotfile="graph")
    src = dotfile + ".dot"
    dot = dotfile + "." + fmt

    File.open(src, 'w') do |f|
      f << self.to_dot_graph.to_s << "\n"
    end

    system( "neato -T#{fmt} #{src} -o #{dot}" )
    dot
  end


end