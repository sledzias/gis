require 'rgl/adjacency'
require 'rgl/dot'
require './coloring'
require './color_vertex'


dg=RGL::AdjacencyGraph.new
dg.extend(COLORING)

dg.load('graph-test.dot')
dg.write_to_graphic_file
dg.transform_graph
dg.lf_coloring
dg.write_to_graphic_file('png', 'graph2')
p 'Liczba uzytych kolorow: ' + dg.max_color.to_s
