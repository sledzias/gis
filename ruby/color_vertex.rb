class ColorVertex
  @id
  @color_id
  @type
  attr_accessor :color_id, :id, :type

  def initialize(id)
    @color_id = nil
    @id = id
  end

  def <=>(anOther)
    @id <=> anOther.id
  end

  def to_s
    @id.to_s
  end

  def to_s_large
    @id.to_s + ' ' + type.to_s
  end

end
