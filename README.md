# Projekt z przedmiotu GIS
**Autorzy**: Piotr Raczkowski, Michał Ślaziński

**Prowadzacy**:	dr hab. inż. Krzysztof Pieńkosz

**Temat**:		Kolorowanie wierzchołków i krawędzi grafu

**Opis**:		Kolorowanie totalne polega na jednoczesnym malowaniu wierzchołków i krawędzi. Zakłada się przy tym, że żadne dwa wierzchołki sąsiednie, żadne dwie krawędzie sąsiednie i żadna krawędź incydentna z wierzchołkiem nie mogą otrzymać tego samego koloru. Przy kolorowaniu dąży się do tego, aby liczba użytych kolorów była jak najmniejsza. W ramach projektu należy opracować algorytm i program do kolorowania totalnego grafu. Literatura: Optymalizacja dyskretna; modele i metody kolorowania grafów, WNT, Warszawa


## Definicje
*	*krawędź incydentna z wierzchołkiem* - krawędź grafu jest incydentna do wierzchołka jeśli ma ona w tym wierzchołku swój koniec lub początek (wychodzi lub wchodzi do tego wierzchołka).

## Założenia

1. Wynik działania programu jest poprawny ale niekoniecznie optymalny.
2. Program ma działać pod platformą Unix (Mac OSX) oraz Windows
3. Program będzie napisany w języku Ruby z użyciem biblioteki RGL ([http://rgl.rubyforge.org](http://rgl.rubyforge.org))


## Algorytmy kolorowania grafów 
Metodą rozwiązania problemu kolorowania totalnego jest sprowadzenie problemu do pokolorowania wierzchołków grafu. Metoda ta będzie składała się z etapów:

1. Przekształcenie problemu kolorowania totalnego na problem kolorowania wierzchołków grafu
2. Pokolorowanie wierzchołków grafu dowolnym algorytmem (w projekcie użyliśmy algorytmu LF (largest first))
3. Powrót do pierwotnego grafu i pokolorowanie krawędzi.

Szczegółowa wersja algorytmu jest następująca:

1. Dla każdej krawędzi q(u,v) zrób:
	1. stwórz nowy wierzchołek w i oznacz go gwiazdką (*). 
	2. Stwórz nowe krawędzie (u,w\*) oraz (v,w\*)
2. Dla każdego wierzchołka v bez gwiazdki zrób:
	1. znajdź wszystkie wierzchołki oznaczone \* sąsiadujące z wierzchołkiem v i zapisz je w zbiorze S
	2. Stwórz dwuelementowe kombinacje wierzchołków zbioru S. Dla każdej kombinacji stwórz nową krawędź
3. Otrzymany graf pokoloruj dowolnym algorytmem kolorowania wierzchołków grafu.
4. Dla każdej krawędzi q(u,v): 
	1. znajdź wierzchołek w, który sąsiaduje z wierzchołkiem u oraz v. 
	2. Krawędzi q przypisz kolor wierzchołka w.	
	3. Usuń z grafu krawędź (u,w) oraz (v,w) 	
5. Usuń wszystkie wierzchołki oznaczone \*

## Interfejs
Plikiem wejściowym i wyjściowym programu będzie plik w formacie dot ( [http://en.wikipedia.org/wiki/DOT_(graph_description_language](http://en.wikipedia.org/wiki/DOT_(graph_description_language) ). Dzięki takiemu rozwiązaniu program będzie kompatybilny z innymi programami operującymi na grafach. Oprócz pliku wyjściowego w formacie dot program będzie generował podgląd grafu w formie obrazka (png). 

### Plik wejściowy
	graph RGL__AdjacencyGraph {
   	 	1 [label = 1]
   	 	2 [label = 2]
   	 	3 [label = 3]
   	 	5 [label = 5]
   	 	4 [label = 4]
   	 	6 [label = 6]
   	 	1 -- 2 []
   	 	1 -- 3 []
   	 	1 -- 5 []
   	 	1 -- 6 []
   	 	2 -- 3 []
   	 	2 -- 4 []
   	 	2 -- 6 []
  	  	5 -- 4 []
    	4 -- 6 []
	}

![graf wejściowy](ruby/graph.png "Graf  wejściowy")

### Plik wyjściowy
	graph RGL__AdjacencyGraph {
    	1 [label = 1, color="#e32636"]
	    2 [label = 2, color="#007fff"]
    	3 [label = 3, color="#915c83"]
	    5 [label = 5, color="#007fff"]
    	4 [label = 4, color="#915c83"]
	    6 [label = 6, color="#fdee00"]
	    1 -- 2 [color="#fdee00"]
    	1 -- 3 [color="#007fff"]
	    1 -- 5 [color="#915c83"]
	    1 -- 6 [color="#008000"]
	    2 -- 3 [color="#e32636"]
   	 	2 -- 4 [color="#008000"]
    	2 -- 6 [color="#915c83"]
    	5 -- 4 [color="#e32636"]
    	4 -- 6 [color="#007fff"]
	}


![pokolorowany graf](ruby/graph-color.png "Pokolorowany graf")

### Uruchomienie programu

W celu uruchomienia programu należy mieć skonfigurowane środowisko interpretera języka ruby 1.9.3 oraz zainstalowany gem bundler (polecenie `gem install bundler`).
Kolejnym krokiem jest instalacja niezbędnych gemów poleceniem `bundle install` wykonanym wewnątrz katalogu _GIS/ruby_ .
Program uruchamia się poleceniem `ruby gis.rb` 



